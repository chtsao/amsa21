---
title: "Hello"
date: 2021-02-18T05:32:32+08:00
draft: false
tags: [why, motif]
---
![](https://static2.lolwallpapers.net/2016/01/Ashe-Fan-Art-1.jpg)

## Motivating Questions

Ranking, Classification, Map, Visualization of 

* LOL Legends:
* Pokemons
* Ingredients of (Food/Recipes)
* Users/Members of Youtube, FB, IG, DCard, etc

## Key Themes: Data-Vis and Virtual-Dim

* DV: Data Visualization
* VD:  Virtual Dimension

### Of Course

[About](https://chtsao.gitlab.io/amsa21/about/): Textbook, Computing






[How to Lie with Charts](https://www.google.com/imgres?imgurl=http%3A%2F%2Fwww.getbulb.com%2Fuploads%2F6%2F3%2F6%2F4%2F63647733%2F6274053.png%3F1447015642&imgrefurl=http%3A%2F%2Fwww.getbulb.com%2Fblog%2Fhow-to-lie-with-charts&tbnid=SFedhe_5jnu1UM&vet=12ahUKEwi547j3pPTuAhVN7ZQKHXfbBDsQMyg1egQIARAp..i&docid=C7uZIabqG3EuAM&w=420&h=381&q=How%20charts%20lie&client=firefox-b-d&ved=2ahUKEwi547j3pPTuAhVN7ZQKHXfbBDsQMyg1egQIARAp)

