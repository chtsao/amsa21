---
title: "Project and Data"
date: 2021-03-13T12:08:14+08:00
draft: false
categories: [note, team, data, game]
tags: [data.frame, data visualization]
---

<img src="https://www.whats-on-netflix.com/wp-content/uploads/2020/11/series-like-the-queens-gambit-on-netflix.jpg" style="zoom:50%;" />

Project data
---
現在網路上資料很多，應該有妳需要或適合做不同等級的資料。但你要找到適當的**關鍵字**來搜索，另外則**篩選**。另外也需要閱讀一些文件、報告、paper。當然，其中幾乎大多數都是英文文件。習慣就好。以下是我搜尋 "data for student projects", "data for data science projects" 的不錯連結。我是以[DuckDuckGo](https://duckduckgo.com/) 搜尋。


* Search at [Medium](https://duckduckgo.com/?t=ffsb&q=medium+data+science+projects&atb=v231-1&ia=web),  [Google](https://www.google.com/search?client=firefox-b-d&q=data+science+projects)
* [6 Complete Data Science Projects](https://www.springboard.com/blog/data-science-projects/). 文尾 *Tips for Creating Cool Data Science Projects* 很值得參考。
* [16 Data Science Project Ideas with Source Code to Strengthen your Resume](https://data-flair.training/blogs/data-science-project-ideas/) 稍高能。Projects in [R](https://data-flair.training/blogs/data-science-r-movie-recommendation/), [Python](https://data-flair.training/blogs/python-project-ideas/)
* [Data Science Career Paths: Different Roles](https://www.springboard.com/blog/data-science-career-paths-different-roles-industry/)


* [18 Places to Find Free Data Sets for Data Science Projects](https://www.dataquest.io/blog/free-datasets-for-projects/)
* [19 Free Public Data Sets for Your Data Science Project](https://www.springboard.com/blog/free-public-data-sets-data-science-project/)
* [25+ websites to find datasets for data science projects](https://www.analyticsvidhya.com/blog/2016/11/25-websites-to-find-datasets-for-data-science-projects/)
* [50+ free Datasets for Data Science Projects](https://blog.journeyofanalytics.com/50-free-datasets-for-data-science-projects/)


## Open Data

* [政府開放資料平台](https://data.gov.tw/)
* [台北市資料大平台](https://data.taipei/)
* [Data Portals](http://dataportals.org/)  A Comprehensive List of Open Data Portals from Around the World
* [Open Data Cube](https://www.opendatacube.org/) The Open Data Cube (ODC) is an Open Source Geospatial Data Management and Analysis Software project that helps you harness the power of Satellite data.

## Taiwan

* [Oceans of Data — g0v.tw and Taiwan’s Open Data Model](https://medium.com/open-and-shut/oceans-of-data-g0v-tw-and-taiwans-open-data-model-6a27f3203b01)

* [Global Open Data Index of Taiwan](https://index.okfn.org/place/)
* [How does a country get to open data? What Taiwan can teach us about the evolution of access](https://www.niemanlab.org/2013/04/how-does-a-country-get-to-open-data-what-taiwan-can-teach-us-about-the-evolution-of-access/)

Reproducible Science
* [Reproducible Science](https://reproduciblescience.org/)
* [Reproducibility (wiki)](https://en.wikipedia.org/wiki/Reproducibility)